<?xml version="1.0" encoding="UTF-8"?><!-- 
== Copyright (c) 2002-2006. All rights reserved. 
== Financial Products Markup Language is subject to the FpML public license. 
== A copy of this license is available at http://www.fpml.org/documents/license
--><xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.bbva.com/FMM-1" xmlns:fpmlc="http://www.fpml.org/FpML-5/confirmation" xmlns:fpmlr="http://www.fpml.org/FpML-5/reporting" version="5-3-6-3-128" targetNamespace="http://www.bbva.com/FMM-1" elementFormDefault="qualified" attributeFormDefault="unqualified">
	<xsd:import namespace="http://www.fpml.org/FpML-5/confirmation" schemaLocation="../confirmation/fpml-main-5-3.xsd"/>
	<xsd:import namespace="http://www.fpml.org/FpML-5/reporting" schemaLocation="../reporting/fpml-main-5-3.xsd"/>
	<xsd:include schemaLocation="fmm-shared.xsd"/>
	<xsd:complexType name="CalculationPeriod">

		<xsd:sequence>

			<xsd:element name="adjustedStartDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Date that defines the beginning of the calculation period.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="adjustedEndDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Date that defines the end of the calculation period.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			
			<xsd:element name="adjustedFixingDate" type="xsd:date" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">The adusted fixing date.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>

		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="CashflowFixingConfirmation">
		<xsd:annotation>
			<xsd:documentation xml:lang="en">Details of the computation of a computed rate or price used to calculate the amount of a cashflow component. This computed rate or price may include averaging and/or various types of rate treatment rules. The details include all of the observations, the calculation parameters, and the resulting value.</xsd:documentation>
		</xsd:annotation>
		<xsd:sequence>
			<xsd:element name="observationReference" type="CashflowObservationReference" maxOccurs="unbounded" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">Reference to the observation details of a particular rate observation.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="calculatedValue" type="xsd:decimal" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The value computed based on averaging the underlying observation and applying any spreads, multipliers, and cap and floors values. average or treated value computed based on the underlyer observations, following the calculation rules.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="calculatedDate" type="xsd:date" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The date when the value is computed.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="multiplier" type="xsd:decimal" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">A rate multiplier to apply to the floating rate. The multiplier can be a positive or negative decimal. This element should only be included if the multiplier is not equal to 1 (one).</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="spread" type="xsd:decimal" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The ISDA Spread, if any, which applies for the calculation period. It also defines spread as price. The spread is a per annum rate, expressed as a decimal. For purposes of determining a calculation period amount, if positive the spread will be added to the floating rate and if negative the spread will be subtracted from the floating rate. A positive 10 basis point (0.1%) spread would be represented as 0.001.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="capValue" type="fpmlc:Strike" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The cap rate or price, if any, which applies to the floating rate for the calculation period. The cap rate (strike) is only required where the floating rate on a swap stream is capped at a certain strike level. The cap rate is assumed to be exclusive of any spread and is a per annum rate, expressed as a decimal. A cap rate of 5% would be represented as 0.05.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="floorValue" type="fpmlc:Strike" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The floor rate or price, if any, which applies to the floating rate for the calculation period. The floor rate (strike) is only required where the floating rate on a swap stream is floored at a certain strike level. The floor rate is assumed to be exclusive of any spread and is a per annum rate, expressed as a decimal. The floor rate of 5% would be represented as 0.05.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
		<xsd:attribute name="id" type="xsd:ID"/>
	</xsd:complexType>
	<xsd:complexType name="CashflowObservationReference">
		<xsd:annotation>
			<xsd:documentation xml:lang="en">Reference to a cash flow obervation component.</xsd:documentation>
		</xsd:annotation>
		<xsd:complexContent>
			<xsd:extension base="fpmlc:Reference">
				<xsd:attribute name="href" type="xsd:IDREF" use="required"/>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="FixingCalculationElements">

		<xsd:sequence>

			<xsd:element name="calculationPeriod" type="CalculationPeriod">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">The period details for calculation/accrual periods that comprise this cashflow component.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="notional" type="fpmlc:Money">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Identifies the notional in effect for this calculation period.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			
			<xsd:element name="underlyer" type="fpmlc:TradeUnderlyer2" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The underlyer(s) used to calculate the amount of this cashflow component.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="adjustedPaymentDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">The date on which this payment will settle.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="customizationForwardOffset" type="xsd:int" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Indicates the flow related to the customization, referenced from first flow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="customizationBackwardOffset" type="xsd:int" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Indicates the flow related to the customization, referenced from last flow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:group ref="LegId.model">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Phase and leg identification in each product stream.</xsd:documentation>

				</xsd:annotation>

			</xsd:group>
			<xsd:element name="calculatedRate" type="CashflowFixingConfirmation" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The computed rate(s) or price(s) used to calculate the amount of this cashflow component. These computed rates or prices may include averaging and/or various types of rate treatment rules.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="FixingCalculationElementsReporting">

		<xsd:sequence>

			<xsd:element name="calculationPeriod" type="CalculationPeriod" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">The period details for calculation/accrual periods that comprise this cashflow component.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="notional" type="fpmlr:Money" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Identifies the notional in effect for this calculation period.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			
			<xsd:element name="underlyer" type="fpmlr:TradeUnderlyer2" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The underlyer(s) used to calculate the amount of this cashflow component.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

			<xsd:element name="adjustedPaymentDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">The date on which this payment will settle.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="customizationForwardOffset" type="xsd:int" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Indicates the flow related to the customization, referenced from first flow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="customizationBackwardOffset" type="xsd:int" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Indicates the flow related to the customization, referenced from last flow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:group ref="LegIdReporting.model">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Phase and leg identification in each product stream.</xsd:documentation>

				</xsd:annotation>

			</xsd:group>
			<xsd:element name="calculatedRate" type="CashflowFixing" minOccurs="0" maxOccurs="unbounded">
				
				<xsd:annotation>
				
					<xsd:documentation xml:lang="en">The computed rate(s) or price(s) used to calculate the amount of this cashflow component. These computed rates or prices may include averaging and/or various types of rate treatment rules.</xsd:documentation>
				
				</xsd:annotation>
			
			</xsd:element>
		
		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="FixingEvent">

		<xsd:complexContent>

			<xsd:extension base="fpmlc:AdditionalEvent">

				<xsd:sequence>

					<xsd:element name="partyTradeIdentifier" type="fpmlc:PartyTradeIdentifier" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">The trade reference identifier(s) allocated to the trade by the parties involved.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="partyTradeInformation" type="PartyTradeInformation" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Additional trade information that may be provided by each involved party.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="productCategorization" type="FixingProductCategorization" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Container for product information.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="instrumentFixing" type="InstrumentFixing" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Fixing details.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>
					
					<xsd:element name="settlementCurrency" type="fpmlc:Currency" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The currency in which cash settlement occurs.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="calculationElements" type="FixingCalculationElements" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="grossCashflow" type="FixingGrossCashflow" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">Payment details of this cash flow component, including currency, amount and payer/receiver.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="calculationDetails" type="FixingCalculationDetails" minOccurs="0" maxOccurs="unbounded">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>

				</xsd:sequence>

			</xsd:extension>

		</xsd:complexContent>

	</xsd:complexType>

	<xsd:complexType name="FixingCalculationDetails">
		
		<xsd:sequence>
			<xsd:element name="settlementCurrency" type="fpmlc:Currency" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">The currency in which cash settlement occurs.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
			<xsd:element name="calculationElements" type="FixingCalculationElements">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
			<xsd:element name="grossCashflow" type="FixingGrossCashflow" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">Payment details of this cash flow component, including currency, amount and payer/receiver.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
		</xsd:sequence>
		
	</xsd:complexType>

	<xsd:complexType name="FixingEventReporting">

		<xsd:complexContent>

			<xsd:extension base="fpmlr:AdditionalEvent">

				<xsd:sequence>

					<xsd:element name="partyTradeIdentifier" type="fpmlr:PartyTradeIdentifier" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">The trade reference identifier(s) allocated to the trade by the parties involved.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="partyTradeInformation" type="PartyTradeInformationReporting" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Additional trade information that may be provided by each involved party.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="productCategorization" type="FixingProductCategorizationReporting" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Container for product information.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>

					<xsd:element name="instrumentFixing" type="InstrumentFixingReporting" minOccurs="0" maxOccurs="unbounded">

						<xsd:annotation>

							<xsd:documentation xml:lang="en">Fixing details.</xsd:documentation>

						</xsd:annotation>

					</xsd:element>
					
					<xsd:element name="settlementCurrency" type="fpmlr:Currency" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The currency in which cash settlement occurs.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="calculationElements" type="FixingCalculationElementsReporting" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="grossCashflow" type="fpmlr:GrossCashflow" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">Payment details of this cash flow component, including currency, amount and payer/receiver.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
					
					<xsd:element name="calculationDetails" type="FixingCalculationDetailsReporting" minOccurs="0" maxOccurs="unbounded">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>

				</xsd:sequence>

			</xsd:extension>

		</xsd:complexContent>

	</xsd:complexType>
	<xsd:complexType name="FixingCalculationDetailsReporting">
		
		<xsd:sequence>
			
			<xsd:element name="settlementCurrency" type="fpmlr:Currency" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">The currency in which cash settlement occurs.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
			<xsd:element name="calculationElements" type="FixingCalculationElementsReporting" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">The set of cash flow components with calculations that comprise this payment.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
			<xsd:element name="grossCashflow" type="fpmlr:GrossCashflow" minOccurs="0">
				
				<xsd:annotation>
					
					<xsd:documentation xml:lang="en">Payment details of this cash flow component, including currency, amount and payer/receiver.</xsd:documentation>
					
				</xsd:annotation>
				
			</xsd:element>
			
		</xsd:sequence>
		
	</xsd:complexType>
	<xsd:complexType name="FixingGrossCashflow">

		<xsd:sequence>

			<xsd:element name="cashflowId" type="fpmlc:CashflowId" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Unique identifier for a cash flow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="partyTradeIdentifierReference" type="fpmlc:PartyTradeIdentifierReference" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Pointer-style reference to the partyTradeIdentifier block within the tradeIdentifyingItems collection, which identifies the parent trade for this cashflow.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:group ref="fpmlc:PayerReceiver.model" minOccurs="0"/>

			<xsd:element name="cashflowAmount" type="fpmlc:Money">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Cash flow amount in a given currency to be paid/received.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="cashflowType" type="fpmlc:CashflowType" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Defines the type of cash flow. For instance, a type of fee, premium, principal exchange, leg fee.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="FixingId">

		<xsd:simpleContent>

			<xsd:extension base="fpmlc:Scheme">

				<xsd:attribute name="fixingIdScheme" type="xsd:anyURI"/>

			</xsd:extension>

		</xsd:simpleContent>

	</xsd:complexType>
	<xsd:complexType name="FixingIdReporting">

		<xsd:simpleContent>

			<xsd:extension base="fpmlr:Scheme">

				<xsd:attribute name="fixingIdScheme" type="xsd:anyURI"/>

			</xsd:extension>

		</xsd:simpleContent>

	</xsd:complexType>
	<xsd:complexType name="FixingProductCategorization">

		<xsd:sequence>

			<xsd:group ref="fpmlc:Product.model" minOccurs="0"/>

		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="FixingProductCategorizationReporting">

		<xsd:sequence>

			<xsd:group ref="fpmlr:Product.model" minOccurs="0"/>

		</xsd:sequence>

	</xsd:complexType>
	<xsd:complexType name="InstrumentFixing">

		<xsd:sequence>

			<xsd:element name="fixingId" type="FixingId" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Fixing reference number.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:choice>
				<xsd:element ref="fpmlc:underlyingAsset">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">Define the underlying asset, either a listed security or other instrument.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

				<xsd:element name="quotedCurrencyPair" type="fpmlc:QuotedCurrencyPair">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">Underlying asset defined as currency pair, with currency1, currency2 and quoteBasis.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

				<xsd:element name="fixedRate" type="fpmlc:Schedule">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">The fixed rate or fixed rate schedule expressed as explicit fixed rates and dates. In the case of a schedule, the step dates may be subject to adjustment in accordance with any adjustments specified in calculationPeriodDatesAdjustments.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

				<xsd:element name="floatingRate" type="fpmlc:FloatingRate">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">A floating rate.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

			</xsd:choice>

			<xsd:element name="fixingDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Describes the specific date when a particular rate is fixed.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="fixingValue" type="xsd:decimal" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Describes the specific rate value fixed.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="fxSpotRateSource" type="fpmlc:FxSpotRateSource" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Specifies the methodology (reference source and, optionally, fixing time) to be used for determining a currency conversion rate.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			<xsd:element name="flex" type="FlexConfirmation" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Cointer to include the user defined fields in flex trades.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			
			<xsd:element name="observationElements" type="FixingObservationElements" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">Fixing details.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			
		</xsd:sequence>

		<xsd:attribute name="id" type="xsd:ID"/>

	</xsd:complexType>
	<xsd:complexType name="FloatingRate">
		<xsd:complexContent>
			<xsd:extension base="fpmlc:FloatingRate">
				<xsd:sequence>
					<xsd:element name="payoutType" type="PayoutType" minOccurs="0">
						
						<xsd:annotation>
							
							<xsd:documentation xml:lang="en">Indicate the type of payout of the operation</xsd:documentation>
							
						</xsd:annotation>
						
					</xsd:element>
				</xsd:sequence>
			</xsd:extension>
		</xsd:complexContent>
	</xsd:complexType>
	<xsd:complexType name="FixingObservationElements">
		<xsd:sequence>
			<xsd:choice>
				<xsd:element ref="fpmlc:underlyingAsset">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">Define the underlying asset, either a listed security or other instrument.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="quotedCurrencyPair" type="fpmlc:QuotedCurrencyPair">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">Underlying asset defined as currency pair, with currency1, currency2 and quoteBasis.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="fixedRate" type="fpmlc:Schedule">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">The fixed rate or fixed rate schedule expressed as explicit fixed rates and dates. In the case of a schedule, the step dates may be subject to adjustment in accordance with any adjustments specified in calculationPeriodDatesAdjustments.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="floatingRate" type="fpmlc:FloatingRate">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">A floating rate.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element name="observationDate" type="xsd:date" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The date when the rate is observed.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="observedValue" type="fpmlc:BasicQuotation" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The observed rate or price, together with descriptive information such as units.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="weight" type="xsd:decimal" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The factor used to weight the observation in computing a weighted average. This is typically based on the number of days weighting to be associated with the rate observation, i.e. the number of days such rate is in effect. This is applicable in the case of a weighted average method of calculation where more than one observate date is established for a single calculation period. If omitted all observations are weighted equally. </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="FixingObservationElementsReporting">
		<xsd:sequence>
			<xsd:choice>
				<xsd:element ref="fpmlr:underlyingAsset" minOccurs="0">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">Define the underlying asset, either a listed security or other instrument.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="fixedRate" type="fpmlr:Schedule" minOccurs="0">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">The fixed rate or fixed rate schedule expressed as explicit fixed rates and dates. In the case of a schedule, the step dates may be subject to adjustment in accordance with any adjustments specified in calculationPeriodDatesAdjustments.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
				<xsd:element name="floatingRate" type="fpmlr:FloatingRate" minOccurs="0">
					<xsd:annotation>
						<xsd:documentation xml:lang="en">A floating rate.</xsd:documentation>
					</xsd:annotation>
				</xsd:element>
			</xsd:choice>
			<xsd:element name="observationDate" type="xsd:date" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The date when the rate is observed.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="observedValue" type="fpmlr:BasicQuotation" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The observed rate or price, together with descriptive information such as units.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>
			<xsd:element name="weight" type="xsd:decimal" minOccurs="0">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">The factor used to weight the observation in computing a weighted average. This is typically based on the number of days weighting to be associated with the rate observation, i.e. the number of days such rate is in effect. This is applicable in the case of a weighted average method of calculation where more than one observate date is established for a single calculation period. If omitted all observations are weighted equally. </xsd:documentation>
				</xsd:annotation>
			</xsd:element>
		</xsd:sequence>
	</xsd:complexType>
	<xsd:complexType name="InstrumentFixingReporting">

		<xsd:sequence>

			<xsd:element name="fixingId" type="FixingIdReporting" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Fixing reference number.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:choice minOccurs="0">

				<xsd:element ref="fpmlr:underlyingAsset">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">Define the underlying asset, either a listed security or other instrument.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

				<xsd:element name="fixedRate" type="fpmlr:Schedule">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">The fixed rate or fixed rate schedule expressed as explicit fixed rates and dates. In the case of a schedule, the step dates may be subject to adjustment in accordance with any adjustments specified in calculationPeriodDatesAdjustments.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

				<xsd:element name="floatingRate" type="fpmlr:FloatingRate">

					<xsd:annotation>

						<xsd:documentation xml:lang="en">A floating rate.</xsd:documentation>

					</xsd:annotation>

				</xsd:element>

			</xsd:choice>

			<xsd:element name="fixingDate" type="xsd:date" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Describes the specific date when a particular rate is fixed.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="fixingValue" type="xsd:decimal" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Describes the specific rate value fixed.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>

			<xsd:element name="fxSpotRateSource" type="fpmlr:FxSpotRateSource" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Specifies the methodology (reference source and, optionally, fixing time) to be used for determining a currency conversion rate.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			<xsd:element name="flex" type="Flex" minOccurs="0">

				<xsd:annotation>

					<xsd:documentation xml:lang="en">Cointer to include the user defined fields in flex trades.</xsd:documentation>

				</xsd:annotation>

			</xsd:element>
			<xsd:element name="observationElements" type="FixingObservationElementsReporting" minOccurs="0" maxOccurs="unbounded">
				<xsd:annotation>
					<xsd:documentation xml:lang="en">Fixing details.</xsd:documentation>
				</xsd:annotation>
			</xsd:element>

		</xsd:sequence>

		<xsd:attribute name="id" type="xsd:ID" use="optional"/>

	</xsd:complexType>

	<xsd:element name="fixing" type="FixingEvent" substitutionGroup="fpmlc:additionalEvent"/>
	<xsd:element name="fixingReporting" type="FixingEventReporting" substitutionGroup="fpmlr:additionalEvent"/>
</xsd:schema>
