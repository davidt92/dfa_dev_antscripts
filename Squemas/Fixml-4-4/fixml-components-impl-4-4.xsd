<?xml version="1.0" encoding="UTF-8"?>
<!--
    Revision: FIXML 4.4 Schema Version - 20040109
    Copyright 2003,2004 FIX Protocol Limited.  All rights reserved.

    This Schema represents FIXML vocabulary based on version 4.4 of the FIX Protocol

    Comments should be posted on the FIX protocol web-site
    www.fixprotocol.org
-->
<xs:schema targetNamespace="http://www.fixprotocol.org/FIXML-4-4" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns="http://www.fixprotocol.org/FIXML-4-4" elementFormDefault="qualified" attributeFormDefault="unqualified">
    <xs:redefine schemaLocation="fixml-components-base-4-4.xsd">
        <xs:attributeGroup name="BaseHeaderAttributes">
            <xs:attributeGroup ref="BaseHeaderAttributes"/>
            <xs:attribute name="MsgTyp" type="MsgType_t" use="optional"/>
        </xs:attributeGroup>
        <xs:attributeGroup name="InstrumentAttributes">
            <xs:attributeGroup ref="InstrumentAttributes"/>
            <xs:attribute name="CpnTyp" type="CouponType_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Coupon type.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CpnFreqPeriod" type="CouponFrequencyPeriod_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Coupon frequency period.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CpnFreqUnit" type="CouponFrequencyUnit_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Coupon frequency unit.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CpnDayCount" type="CouponDayCount_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Coupon day count.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="SecID" type="BenchmarkSecurityID_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Benchmark security ID.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="SecIDSrc" type="BenchmarkSecurityIDSource_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Benchmark security ID Source.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="ExerStyle" type="ExerciseStyle_t" use="optional"/>
            <xs:attribute name="PutCall" type="PutOrCall_t" use="optional"/>
            <xs:attribute name="CmpoQnto" type="CompoQuanto_t" use="optional"/>
            <xs:attribute name="FaceVal" type="FaceValue_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Face Value per Unit, omitted if UnderlyingQty=1 (request for 1:1 option).</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="UnitCalc" type="UnitCalc_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Convention for unit calculation.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="SettlDtOfst" type="SettlDateOffset_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of business days according to settlement calendar between the initial fixing of the instrument and the payment.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="RedDtOfst" type="RedemptionDateOffset_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of business days according to settlement calendar between the final fixing of the instrument and the repayment.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="PeriodCalcFreq" type="PeriodCalculationFrequency_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Frequency according to which the coupon and/or autocall periods are paid/observed.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="FnlSttlTyp" type="FinalSettlType_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Type of Final Settlement of product.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CutOff" type="CutOff_t" use="optional">
                <xs:annotation>
                    <xs:documentation>FX Daily Cut-Off; set only in case of FX product.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CpnStyle" type="CouponStyle_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Only for coupon products. Indicates whether the coupon is fixed per period.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="AsianCalcTyp" type="AsianCalcType_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Asian calculation type.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="Callblt" type="Callability_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Whether the product is callable or not.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="MemCpn" type="MemoryCoupon_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Memory Coupon.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CcyRt" type="CurrencyRate_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Currency depo rate for this instrument; set only in case of FX product.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CnvrsnCcyRt" type="ConversionCurrencyRate_t" use="optional">
                <xs:annotation>
                    <xs:documentation>ConversionCurrency depo rate for this instrument; set only in case of FX product.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="InScope" type="InScope_t" use="optional">
                <xs:annotation>
                    <xs:documentation>In Scope; Required for instruments referencing at least one US Stock.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="PaydByIssr" type="PayedByIssuer_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Payed by issuer; Required for instruments referencing at least one US Stock.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="BrrHitProb" type="BarrierHitProbability_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Barrier hit probability of the instrument - for barrier products only.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CpnAmtDec" type="CouponAmtDecimals_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of Decimals to which the paid coupon amount should be rounded - for counpon products only.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="CnvrsnCcy" type="ConversionCurrency_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Indicates currency other than product currency; set only in case of FX product.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="DayCnt" type="DayCount_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Day count only applicable for floating leg (Coupons are fix per period, no DayCount).</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="Flag" type="Flag_t" use="optional"/>              
        </xs:attributeGroup>       
        <xs:group name="InstrumentElements">
            <xs:sequence>
                <xs:group ref="InstrumentElements"/>
                <xs:element name="ExotEvnt" type="ExoticEventsGrp_Block_t" minOccurs="0" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:group>   
        <xs:attributeGroup name="PegInstructionsAttributes">
            <xs:attributeGroup ref="PegInstructionsAttributes"/>
            <xs:attribute name="PegPxTyp" type="PegPriceType_t" use="optional"/>
        </xs:attributeGroup>
        <xs:attributeGroup name="PartiesAttributes">
            <xs:attributeGroup ref="PartiesAttributes"/>
            <xs:attribute name="Qual" type="PartyRoleQualifier_t" use="optional"/>
        </xs:attributeGroup>
        <xs:attributeGroup name="InstrumentLegAttributes">
            <xs:attributeGroup ref="InstrumentLegAttributes"/>
            <xs:attribute name="PxTyp" type="InstrumentLegPriceType_t" use="optional"/>
            <xs:attribute name="ClrFclty" type="InstrumentLegClearingFacility_t" use="optional"/>
            <xs:attribute name="SettlMeth" type="InstrumentLegSettlMethod_t" use="optional"/>
            <xs:attribute name="CumQty" type="LegCumQty_t" use="optional"/>
            <xs:attribute name="LeavesQty" type="LegLeavesQty_t" use="optional"/>
            <xs:attribute name="AvgPx" type="LegAvgPx_t" use="optional"/>
            <xs:attribute name="LastMkt" type="LegLastMkt_t" use="optional"/>
            <xs:attribute name="TrdgVenuRegTrdID" type="TradingVenueRegulatoryTradeID_t" use="optional"/>
            <xs:attribute name="PutCall" type="LegPutOrCall_t" use="optional"/>
            <xs:attribute name="ExerStyle" type="LegExerciseStyle_t" use="optional"/>
            <xs:attribute name="StrkTyp" type="LegStrikeType_t" use="optional"/>
            <xs:attribute name="PosTyp" type="LegPositionType_t" use="optional"/>
            <xs:attribute name="SettlPxTyp" type="LegSettlPriceType_t" use="optional"/>
            <xs:attribute name="PxDeltaInd" type="LegPriceDeltaIndicator_t" use="optional"/>
            <xs:attribute name="GmmInd" type="LegGammaIndicator_t" use="optional"/>
            <xs:attribute name="BidVolInd" type="LegBidVolatilityIndicator_t" use="optional"/>
            <xs:attribute name="OfrVolInd" type="LegOfferVolatilityIndicator_t" use="optional"/>
            <xs:attribute name="PxDelta" type="LegPriceDelta_t" use="optional"/>
            <xs:attribute name="BidVol" type="LegBidVolatility_t" use="optional"/>
            <xs:attribute name="OfrVol" type="LegOfferVolatility_t" use="optional"/>
            <xs:attribute name="Gmma" type="LegGamma_t" use="optional"/>
            <xs:attribute name="Rho" type="LegRho_t" use="optional"/>
            <xs:attribute name="Thet" type="LegTheta_t" use="optional"/>
            <xs:attribute name="Veg" type="LegVega_t" use="optional"/>
            <xs:attribute name="VolInd" type="LegVolatilityIndicator_t" use="optional"/>
            <xs:attribute name="StrkEnd" type="LegStrikeEndPrice_t" use="optional"/>
            <xs:attribute name="Vol" type="LegVolatility44_t" use="optional"/>
        </xs:attributeGroup>
        <xs:attributeGroup name="InstrmtLegExecGrpAttributes">
            <xs:attributeGroup ref="InstrmtLegExecGrpAttributes"/>
            <xs:attribute name="LastQty" type="LegLastQty_t" use="optional"/>
            <xs:attribute name="PxTyp" type="LegPriceType_t" use="optional"/>              
            <xs:attribute name="ClrFclty" type="LegClearingFacility_t" use="optional"/>
            <xs:attribute name="SettlMeth" type="LegSettlMethod_t" use="optional"/>                       
        </xs:attributeGroup>  
        <xs:attributeGroup name="UnderlyingInstrumentAttributes">
            <xs:attributeGroup ref="UnderlyingInstrumentAttributes"/>
            <xs:attribute name="DeltaHdg" type="DeltaHedge_t" use="optional"/>
            <xs:attribute name="PxRefTyp" type="UnderlyingPxRefType_t" use="optional"/>
            <xs:attribute name="RefSecID" type="UnderlyingRefSecurityID_t" use="optional"/>
            <xs:attribute name="RefSecIDSrc" type="UnderlyingRefSecurityIDSource_t" use="optional"/>
            <xs:attribute name="RefCntactSz" type="UnderlyingRefContractSize_t" use="optional"/>
          
            <xs:attribute name="StrkngDt" type="UdlStrikingDate_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Date at which Strikes etc, are fixed</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="StrkngTm" type="UdlStrikingTime_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Time at which Strikes are fixed</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="QtyCalc" type="UdlQtyCalc_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Convention for calculation UnderlyingQty out of FaceValue.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="PxDec" type="UnderlyingDecimals_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of decimals to which UnderlyingPx should be rounded.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="StrkDec" type="UnderlyingStrikeDecimals_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of decimals to which Strikes, Barriers, etc. should be rounded.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="DeltaAtIss" type="DeltaAtIssue_t" use="optional">
                <xs:annotation>
                    <xs:documentation>Number of underlying shares that constitute the delta/initial hedge of a contract per underlying.</xs:documentation>
                </xs:annotation>
            </xs:attribute>
            <xs:attribute name="MinTrdVol" type="UdlMinTradeVol_t" use="optional"/>
            <xs:attribute name="MaxTrdVol" type="UdlMaxTradeVol_t" use="optional"/>
            <xs:attribute name="Stat" type="UdlStatus_t" use="optional"/>
            <xs:attribute name="TrdStartTm" type="UdlTradeStartTime_t" use="optional"/>
            <xs:attribute name="TrdEndTm" type="UdlTradeEndTime_t" use="optional"/>
            <xs:attribute name="SimStartTm" type="UdlSimulationStartTime_t" use="optional"/>
            <xs:attribute name="SimEndTm" type="UdlSimulationEndTime_t" use="optional"/>
            <xs:attribute name="MaxStrkngOfst" type="UdlMaxStrikingOffset_t" use="optional"/>
            <xs:attribute name="MinMat" type="UdlMinMaturity_t" use="optional"/>
            <xs:attribute name="MaxMat" type="UdlMaxMaturity_t" use="optional"/>
        </xs:attributeGroup>
        <xs:group name="UnderlyingInstrumentElements">
            <xs:sequence>
                <xs:group ref="UnderlyingInstrumentElements"/>
                <xs:element name="ExotEvnt" type="UdlExoticEventsGrp_Block_t" minOccurs="0" maxOccurs="unbounded"/>
            </xs:sequence>
        </xs:group>   
    </xs:redefine>    
    <xs:group name="DisplayInstructionElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="DisplayInstructionAttributes">
        <xs:attribute name="DisplayQty" type="DisplayQty_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="DisplayInstruction_Block_t" final="#all">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="DisplayInstruction" ComponentType="Block"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="DisplayInstructionElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="DisplayInstructionAttributes"/>
    </xs:complexType>
    <xs:group name="TriggeringInstructionElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="TriggeringInstructionAttributes">
        <xs:attribute name="TrgrTyp" type="TriggerType_t" use="optional"/>
        <xs:attribute name="TrgrActn" type="TriggerAction_t" use="optional"/>
        <xs:attribute name="TrgrScope" type="TriggerScope_t" use="optional"/>
        <xs:attribute name="TrgrPx" type="TriggerPrice_t" use="optional"/>
        <xs:attribute name="TrgrSym" type="TriggerSymbol_t" use="optional"/>
        <xs:attribute name="TrgrSecID" type="TriggerSecurityID_t" use="optional"/>
        <xs:attribute name="TrgrSecIDSrc" type="TriggerSecurityIDSource_t" use="optional"/>
        <xs:attribute name="TrgrSecDesc" type="TriggerSecurityDesc_t" use="optional"/>
        <xs:attribute name="TrgrPxTyp" type="TriggerPriceType_t" use="optional"/>
        <xs:attribute name="TrgrPxTypScp" type="TriggerPriceTypeScope_t" use="optional"/>
        <xs:attribute name="TrgrPxDir" type="TriggerPriceDirection_t" use="optional"/>
        <xs:attribute name="TrgrNewPx" type="TriggerNewPrice_t" use="optional"/>
        <xs:attribute name="TrgrOrdTyp" type="TriggerOrderType_t" use="optional"/>
        <xs:attribute name="TrgrNewQty" type="TriggerNewQty_t" use="optional"/>
        <xs:attribute name="TrgrTrdSessID" type="TriggerTradingSessionID_t" use="optional"/>
        <xs:attribute name="TrgrTrdSessSubID" type="TriggerTradingSessionSubID_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="TriggeringInstruction_Block_t" final="#all">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="TriggeringInstruction" ComponentType="Block"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="TriggeringInstructionElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="TriggeringInstructionAttributes"/>
    </xs:complexType>
    <xs:group name="OrderAttributeGrpElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="OrderAttributeGrpAttributes">
        <xs:attribute name="Typ" type="OrderAttributeType_t" use="optional"/>
        <xs:attribute name="Val" type="OrderAttributeValue_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="OrderAttributeGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="OrderAttributeGrp" ComponentType="BlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="OrderAttributeGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="OrderAttributeGrpAttributes"/>
    </xs:complexType>
    
    <xs:group name="RegulatoryTradeIDGrpElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="RegulatoryTradeIDGrpAttributes">
        <xs:attribute name="ID" type="RegulatoryTradeID_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="RegulatoryTradeIDGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="RegulatoryTradeIDGrp" ComponentType="BlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="RegulatoryTradeIDGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="RegulatoryTradeIDGrpAttributes"/>
    </xs:complexType>
    <xs:group name="TrdRegPublicationGrpElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="TrdRegPublicationGrpAttributes">
        <xs:attribute name="Typ" type="TrdRegPublicationType_t" use="optional"/>
        <xs:attribute name="Rsn" type="TrdRegPublicationReason_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="TrdRegPublicationGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="TrdRegPublicationGrp" ComponentType="BlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="TrdRegPublicationGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="TrdRegPublicationGrpAttributes"/>
    </xs:complexType>
    <xs:group name="TradePriceConditionGrpElements">
        <xs:sequence/>
    </xs:group>
    <xs:attributeGroup name="TradePriceConditionGrpAttributes">
        <xs:attribute name="TrdPxCond" type="TradePriceCondition_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="TradePriceConditionGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="TradePriceConditionGrp" ComponentType="BlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="TradePriceConditionGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="TradePriceConditionGrpAttributes"/>
    </xs:complexType>  
    
    <xs:group name="ExoticEventsGrpElements">
        <xs:sequence>
            <xs:element name="NumVal" type="ExotNumValuesGrp_Block_t" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:group>
    <xs:attributeGroup name="ExoticEventsGrpAttributes">
        <xs:attribute name="EvntID" type="ExotEventId_t" use="optional"/>
        <xs:attribute name="EvntDt" type="ExotEventDate_t" use="optional"/>
        <xs:attribute name="EvntEndDt" type="ExotEventEndDate_t" use="optional"/>
        <xs:attribute name="EvntTyp" type="ExotEventType_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="ExoticEventsGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="ExoticEventsGrp" ComponentType="ImplicitBlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="ExoticEventsGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="ExoticEventsGrpAttributes"/>
    </xs:complexType>
    
    <xs:group name="ExotNumValuesGrpElements">
        <xs:sequence/>           
    </xs:group>
    <xs:attributeGroup name="ExotNumValuesGrpAttributes">
        <xs:attribute name="Typ" type="ExotValueType_t" use="optional"/>
        <xs:attribute name="Val" type="ExotValue_t" use="optional"/>       
    </xs:attributeGroup>
    <xs:complexType name="ExotNumValuesGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="ExotNumValues" ComponentType="ImplicitBlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="ExotNumValuesGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="ExotNumValuesGrpAttributes"/>
    </xs:complexType>
    
    <xs:group name="UdlExoticEventsGrpElements">
        <xs:sequence>
            <xs:element name="EvntNumVal" type="UdlExotEventNumValuesGrp_Block_t" minOccurs="0" maxOccurs="unbounded"/>
        </xs:sequence>
    </xs:group>
    <xs:attributeGroup name="UdlExoticEventsGrpAttributes">
        <xs:attribute name="EvntID" type="UdlExotEventId_t" use="optional"/>
        <xs:attribute name="EvntDt" type="UdlExotEventDate_t" use="optional"/>
        <xs:attribute name="EvntEndDt" type="UdlExotEventEndDate_t" use="optional"/>
        <xs:attribute name="EvntTyp" type="UdlExotEventType_t" use="optional"/>
        <xs:attribute name="ValuDirctn" type="UdlExotValueDirection_t" use="optional"/>
        <xs:attribute name="ValuStyle" type="UdlExotValueStyle_t" use="optional"/>
        <xs:attribute name="EvntMonTyp" type="UdlExotEventMonType_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="UdlExoticEventsGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="UnderlyingExoticEventsGrp" ComponentType="ImplicitBlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="UdlExoticEventsGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="UdlExoticEventsGrpAttributes"/>
    </xs:complexType>
    
    <xs:group name="UdlExotEventNumValuesGrpElements">
        <xs:sequence/>           
    </xs:group>
    <xs:attributeGroup name="UdlExotEventNumValuesGrpAttributes">
        <xs:attribute name="Typ" type="UdlExotEventValueType_t" use="optional"/>
        <xs:attribute name="Val" type="UdlExotEventValue_t" use="optional"/>       
    </xs:attributeGroup>
    <xs:complexType name="UdlExotEventNumValuesGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="UdlExotEventNumValuesGrp" ComponentType="ImplicitBlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="UdlExotEventNumValuesGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="UdlExotEventNumValuesGrpAttributes"/>
    </xs:complexType>
    
    <xs:group name="NumQuoteGrpElements">
        <xs:sequence/>           
    </xs:group>
    <xs:attributeGroup name="NumQuoteGrpAttributes">
        <xs:attribute name="QtedPx" type="QuotedPrice_t" use="optional"/>
        <xs:attribute name="QtedPxQteTyp" type="QuotedPriceQteType_t" use="optional"/>       
        <xs:attribute name="QtedRcvTm" type="QuotedReceiveTime_t" use="optional"/>
    </xs:attributeGroup>
    <xs:complexType name="NumQuoteGrp_Block_t">
        <xs:annotation>
            <xs:appinfo xmlns:x="http://www.fixprotocol.org/fixml/metadata.xsd">
                <xs:Xref Protocol="FIX" name="NumQuoteGrp" ComponentType="ImplicitBlockRepeating"/>
                <xs:Xref Protocol="ISO_15022_XML"/>
            </xs:appinfo>
        </xs:annotation>
        <xs:sequence>
            <xs:group ref="NumQuoteGrpElements"/>
        </xs:sequence>
        <xs:attributeGroup ref="NumQuoteGrpAttributes"/>
    </xs:complexType>
    
</xs:schema>
